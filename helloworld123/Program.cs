﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloworld123
{
    class Program
    {
        static void Main(string[] args)

        {
            #region Dino Stuff

            Console.WriteLine("---------------------DINO STUFF BEGINS ---------------------");

            //create a basic dino object.
            var tempDinosaur1 = new Dinosaur();

            var tempDinoString = tempDinosaur1.ToString();

            Console.WriteLine(tempDinoString);


            // Console.WriteLine("Name: "+dinosaur.name+ "Height; "+dinosaur.height+" weight; "+dinosaur.weight+" terrain: "+dinosaur.terrain);


            string tempDinoName = "Triceratops";
            int tempDinoHeight = 10;
            int tempDinoWeight = 12;
            string tempDinoTerrain = "North America";
            int tempDinoID = 1;

            var tempDinosaur2 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            var tempDinoString2 = tempDinosaur2.ToString();

            //Dinosaur tempDinosaur3 = new Dinosaur();
            //tempDinosaur3 = tempDinosaur3.input();
            //tempDinosaur3.display();
            #endregion

            #region Dino Stuff 2

            //TODO - Collections LINQ and lambda 

            //add a unique id to each dino.

            //Collection of Dino Stuff
            var CollectionOfDino = new List<Dinosaur>();
            

            CollectionOfDino.Add(tempDinosaur1);
            CollectionOfDino.Add(tempDinosaur2);

            //we need some ten dinos. so 8 more.
            CollectionOfDino = AddTenDinos();
            var CollectionOfDino_sort = CollectionOfDino.OrderByDescending(x => x.height).ToList();
            //Select(x => x.height).OrderByDescending();

            // CollectionOfDino.Add(tempDinosaur3);

            //Add some dinos to this collection

           // DisplayDinoCollection(CollectionOfDino);
            //Console.WriteLine("Sorted");
            //Console.WriteLine("******************************");
            //Console.WriteLine("******************************");
            //Console.WriteLine("******************************");
            //Console.WriteLine("******************************");
           // DisplayDinoCollection(CollectionOfDino_sort);


            //show dinos based on country

            //showDinoBasedOnCountry(CollectionOfDino);

            showDinoBasedOnCountry("IN", CollectionOfDino);

            addNewDino(CollectionOfDino);
            //make sure that no dino has more than one id.

            //after every addition show the collection

            //search a dino based on id

            //remove a dino. 
            deleteDino(CollectionOfDino);
            //

            #endregion



            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
           
        }

        private static void deleteDino(List<Dinosaur> collectionOfDino)
        {
            Console.WriteLine("");
        }



        #region Show dino based on country
        private static void showDinoBasedOnCountry(List<Dinosaur> collectionOfDino)
        {
            var dinoIn = collectionOfDino.Select(x => x.terrain == "IN").ToList();
            
            var dinoAm = collectionOfDino.Select(x => x.terrain == "AM").ToList();
            
            var dinoCh = collectionOfDino.Select(x => x.terrain == "CH").ToList();
          

            var dinoInList = collectionOfDino.Select(x => x).Where(x => x.terrain == "IN").ToList();
            var dinoAmList = collectionOfDino.Select(x => x).Where(x => x.terrain == "AM").ToList();
            var dinoChList = collectionOfDino.Select(x => x).Where(x => x.terrain == "CH").ToList();
            DisplayDinoCollection(dinoInList);
            DisplayDinoCollection(dinoAmList);
            DisplayDinoCollection(dinoChList);

            

            var dummy = 0;
        }

        private static void showDinoBasedOnCountry(string country, List<Dinosaur> collectionOfDino)
        {
            Console.WriteLine("input country");
            country = Console.ReadLine();
            Console.WriteLine("\nDino found in terrain:"+country+"\n");
            var dinoList = collectionOfDino.Select(x => x).Where(x => x.terrain == country).ToList();
            DisplayDinoCollection(dinoList);
        }
        #endregion

        #region display dino collection
        static void DisplayDinoCollection(List<Dinosaur> collectionOfDino)
        {
            //throw new NotImplementedException();

            //get total dinos.
            var totalDinos = collectionOfDino.Count;

            var message = "inpud dino id to be deleted";

            
            //display total dinos for reference
            message = "Total Number of Dinos - " + totalDinos;
            Console.WriteLine(message);

            //loop through each dino.
            foreach (var dino in collectionOfDino)
            {
                //display dino details using the already existing display function
                dino.display();

                //put a simple line to indicate that a new dino will be displated in the next iteration
                message = "--------------------";
                Console.WriteLine(message);
            }
        }
        #endregion

        #region Add 10 dinos
        private static List<Dinosaur> AddTenDinos()
        {
            string tempDinoName = "Brachosaur";
            int tempDinoHeight = 122;
            int tempDinoWeight = 123;
            string tempDinoTerrain = "IN";
            int tempDinoID = 1;

            var Dino1 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "raptor";
            tempDinoHeight = 190;
            tempDinoWeight = 129;
            tempDinoTerrain = "IN";
            tempDinoID = 2;

            var Dino2 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "t-rex";
            tempDinoHeight = 109;
            tempDinoWeight = 192;
            tempDinoTerrain = "AM";
            tempDinoID = 3;

            var Dino3 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Acristavus";
            tempDinoHeight = 210;
            tempDinoWeight = 192;
            tempDinoTerrain = "AM";
            tempDinoID = 4;

            var Dino4 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "cyclops";
            tempDinoHeight = 240;
            tempDinoWeight = 142;
            tempDinoTerrain = "AM";
            tempDinoID = 5;

            var Dino5 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "branchosaur";
            tempDinoHeight = 410;
            tempDinoWeight = 412;
            tempDinoTerrain = "IN";
            tempDinoID = 6;

            var Dino6 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "quadceratops";
            tempDinoHeight = 422;
            tempDinoWeight = 142;
            tempDinoTerrain = "CH";
            tempDinoID = 7;

            var Dino7 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "boboceratops";
            tempDinoHeight = 140;
            tempDinoWeight = 412;
            tempDinoTerrain = "CH";
            tempDinoID = 8;

            var Dino8 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "boredceratops";
            tempDinoHeight = 130;
            tempDinoWeight = 124;
            tempDinoTerrain = "CH";
            tempDinoID = 9;

            var Dino9 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "killosaur";
            tempDinoHeight = 1220;
            tempDinoWeight = 142;
            tempDinoTerrain = "IN";
            tempDinoID = 10;

            var Dino10 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "topbottom";
            tempDinoHeight = 130;
            tempDinoWeight = 123;
            tempDinoTerrain = "CH";
            tempDinoID = 11;

            //add all dinos to a collection

            var toReturnDinoCollectiono = new List<Dinosaur>();
            toReturnDinoCollectiono.Add(Dino1);
            toReturnDinoCollectiono.Add(Dino2);
            toReturnDinoCollectiono.Add(Dino3);
            toReturnDinoCollectiono.Add(Dino4);
            toReturnDinoCollectiono.Add(Dino5);
            toReturnDinoCollectiono.Add(Dino6);
            toReturnDinoCollectiono.Add(Dino7);
            toReturnDinoCollectiono.Add(Dino8);
            toReturnDinoCollectiono.Add(Dino9);
            toReturnDinoCollectiono.Add(Dino10);

            //return the collection
            return toReturnDinoCollectiono; ;
        }
        #endregion

        #region add new dino
        private static void addNewDino(List<Dinosaur> collectionOfDino)
        {
            int id = collectionOfDino.Count + 1; 
            

            
        }
        #endregion


        #region basic stuff
        static void basicstuff()
        {
            Console.WriteLine("Hello World!");


            #region basic int stuff

            //int a = 5;
            //int b = a + 2; //OK

            //bool test = true;

            //// Error. Operator '+' cannot be applied to operands of type 'int' and 'bool'.
            //int c = a + test;

            // Keep the console window open in debug mode.

            #endregion

            #region basic string stuff

            String string1 = "Exciting times ";
            String string2 = "lie ahead of us";

            String combineTheTwoStrings = string1 + string2;

            Console.WriteLine(combineTheTwoStrings);

            #endregion

            #region basic bool stuff. 

            //taking input
            Console.WriteLine("Enter a number please");
            //var input = Console.ReadLine().ToString();
            var input = "5";

            //converting the input (it will be in string format) to a int type

            int number = 0;
            try
            {
                number = Convert.ToInt32(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("Got some error - {0}", e.ToString());
                //assign a default number in case of error to resume code flow
                number = 10;
            }


            //we need a bool flag. 
            bool flag;

            if (number > 5)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }

            //lets display the value of the flag in the output.
            Console.WriteLine("The value of flag is {0}", flag);

            #endregion

            

            #region Function call 
            function1();
            function2("Rahul");
            Console.WriteLine(function3("RAHUL"));
           
        }

        static void function1()

        {
            Console.WriteLine("Function1");
        }

        static void function2(string name)
        {
            Console.WriteLine("Data  entered in function: " +name);
        }

        static string function3(string name)
        {

            return "Hello "+name;
        }
        #endregion

    }
        #endregion


}
