﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloworld123
//Dinosaur is a class that contains data about the Extinct Reptiles Dinosaur 
//                            ___......__ _
//                        _.-'           ~-_       _.=a~~-_
//--=====-.-.-_----------~   .--.       _   -.__.-~ ( ___===>
//              '''--...__  (    \ \\\ { )       _.-~
//                        =_ ~_  \\-~~~//~~~~-=-~
//                         |-=-~_ \\   \\
//                         |_/   =. )   ~}
//                         |}      ||
//                        //       ||
//                      _//        {{
//                   '='~'          \\_   
//                                   ~~'=                    
{
     class Dinosaur
    {
        public string name { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public string terrain { get; set; }
        public int id { get; set; }


        //TODO

        //default constructor
        #region Constr
       public Dinosaur()
        {
            name = "some name";
            height = 100;
            weight = 100;
            terrain = "some terrain";
            id = 0;
        }
        #endregion
        //custom constructor with parameters
        #region Custom constructor
        public Dinosaur(string name,int height,int weight,string terrain,int id)
        {
            this.name = name;
            this.height = height;
            this.weight = weight;
            this.terrain = terrain;
            this.id = id;
        }
        #endregion
        //function do take input for Dino
        #region input dino
        public Dinosaur input()
        {
            Console.WriteLine("Welcome to Dinosaur Creation lab");
            Console.WriteLine("You are creating custom dinosaur");

            Console.WriteLine("Input Name of the dino u want to create");
            name=Console.ReadLine();
            try
            {
                Console.WriteLine("Input height of the dino u want to create");
                height = int.Parse(Console.ReadLine());

                Console.WriteLine("Input weight of the dino u want to create");
                weight = int.Parse(Console.ReadLine());

                Console.WriteLine("Input ID of the Dinosaur");
                id = int.Parse(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine("Wrong input for height or weight or ID");
            }
         

            Console.WriteLine("Input Terrain of the dino u want to create");
            terrain=Console.ReadLine();


            Dinosaur dino = new Dinosaur(name, height, weight, terrain, id);
            return dino;
            //return new Dinosaur(name, height, weight, terrain); 
        }
        #endregion
        //function to display Dino
        #region
        public void display()
        {
            Console.WriteLine("Name:   "     + this.name 
                          + "\nHeight: "     + this.height
                          + "\nweight: "     + this.weight 
                          + "\nterrain:"     + this.terrain
                          + "\nID:     "     + this.id
                          + "\n");
        }
        #endregion
        //function to print all Dino Names

    }
}
